from django import forms
from .models import Kegiatan, Orang

class KegiatanForm(forms.ModelForm):

    class Meta:
        model = Kegiatan
        fields = ('nama_kegiatan',)
    
    nama_kegiatan = forms.CharField(max_length=25, label='Nama Kegiatan')

class OrangForm(forms.ModelForm):

    class Meta:
        model = Orang
        fields = ('nama_orang', 'kegiatan')
    
    nama_orang = forms.CharField(max_length=25, label='')