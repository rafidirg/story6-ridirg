from django.test import TestCase, Client
from .models import Kegiatan, Orang

class TestStory(TestCase):
    def setUp(self):
        Kegiatan.objects.create(nama_kegiatan="keg 1")
        Kegiatan.objects.create(nama_kegiatan="keg 2")
        Orang.objects.create(nama_orang="saya", kegiatan=Kegiatan.objects.get(nama_kegiatan="keg 1"))
        Orang.objects.create(nama_orang="kamu", kegiatan=Kegiatan.objects.get(nama_kegiatan="keg 2"))

    def test_apakah_url_listkegiatan_ada(self):
        respon = self.client.get('')
        self.assertEquals(respon.status_code, 200)

    def test_apakah_url_formkegiatan_ada(self):
        respon = self.client.get('/formkegiatan/')
        self.assertEquals(respon.status_code, 200)

    def test_toString_dari_orang_dan_kegiatan(self):
        orang = str(Orang.objects.all()[0])
        keg = str(Kegiatan.objects.all()[0])
        self.assertEquals(orang, 'saya')
        self.assertEquals(keg, 'keg 1')

    def test_membuat_kegiatan(self):
        total_keg = Kegiatan.objects.all().count()
        keg = Kegiatan.objects.create(nama_kegiatan="Kegiatan apa ya")
        self.assertEquals(Kegiatan.objects.all().count(), total_keg+1)

    def test_membuat_orang(self):
        total_orang = Kegiatan.objects.all().count()
        orang = Orang.objects.create(nama_orang="watashi", kegiatan=Kegiatan.objects.get(nama_kegiatan="keg 1"))
        self.assertEquals(Orang.objects.all().count(), total_orang+1)

    def test_buat_kegiatan_dari_web(self):
        total_keg = Kegiatan.objects.all().count()
        response = self.client.post('/formkegiatan/', data = {'nama_kegiatan':"kegiatan seru"})
        self.assertEqual(Kegiatan.objects.all().count(), total_keg+1)
        self.assertEqual(response.status_code, 302)

    def test_buat_orang_dari_web(self):
        total_orang = Orang.objects.all().count()
        response = self.client.post('/', data = {'nama_orang':"anata", 'kegiatan':'1'})
        self.assertEqual(Orang.objects.all().count(), total_orang+1)
        self.assertEqual(response.status_code, 302)

    def test_hapus_kegiatan_dari_laman(self):
        total_keg = Kegiatan.objects.all().count()
        response = self.client.get(f'/deletekeg/{Kegiatan.objects.all()[0].id}/')
        self.assertEqual(Kegiatan.objects.all().count(), total_keg-1)
        self.assertEqual(response.status_code, 302)

    def test_hapus_orang_dari_laman(self):
        total_orang = Orang.objects.all().count()
        response = self.client.get(f'/deleteorg/{Orang.objects.all()[0].id}/')
        self.assertEqual(Orang.objects.all().count(), total_orang-1)
        self.assertEqual(response.status_code, 302)