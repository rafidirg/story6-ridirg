from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=25)

    def __str__(self):
        return self.nama_kegiatan

class Orang(models.Model):
    nama_orang = models.CharField(max_length=25)    
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nama_orang