from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import KegiatanForm, OrangForm
from .models import Kegiatan, Orang

def kegiatanku_list(req):
    if req.method == 'POST':
        form_orang = OrangForm(req.POST)
        print(req.POST)
        if form_orang.is_valid():
            form_orang.save()
            return redirect('/')

    kegiatans = Kegiatan.objects.all()
    orangs = Orang.objects.all()
    form_orang = OrangForm()
    return render(req, 'listkegiatan.html', {'kegiatans':kegiatans, 'orangs':orangs, 'form_orang':form_orang})

def kegiatanku_form(req):
    if req.method == 'POST':
        form_kegiatan = KegiatanForm(req.POST)
        if form_kegiatan.is_valid():
            form_kegiatan.save()
            return redirect('/')

    form_kegiatan = KegiatanForm()
    return render(req, 'formkegiatan.html', {'form_kegiatan':form_kegiatan})

def delete_kegiatan(req, id_kegiatan):
    delet = Kegiatan.objects.get(id=id_kegiatan)
    delet.delete()
    return redirect('/')

def delete_orang(req, id_org):
    delet = Orang.objects.get(id=id_org)
    delet.delete()
    return redirect('/')